<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ include file="./init.jsp"%>

<script src="${cdn_url}/common/js/keycloak.js" type="text/javascript"></script>
<script src="${cdn_url}/common/js/bss-min-1.2.6.js"></script>
<link href="${cdn_url}/ccp/css/common.css" rel="stylesheet" />

<div>
    <d4s-boot-2 context="${encoded_context}" 
        gateway="${gateway}"
        redirect-url="${redirect_url}"
        url="${auth_url}"/>
        <script src="${cdn_url}/boot/d4s-boot.js"></script>

        <span>
            Run your Methods/Algorithms on the Cloud
        <!-- </span>
            <span style="display: block; text-align: right;"> -->
            <a href="https://dev.d4science.org/ccp" target="_blank">Learn more</a>
        </span>
    </d4s-boot-2>
</div>
