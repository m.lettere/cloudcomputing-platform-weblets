<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ include file="./init.jsp"%>

<div>
    <d4s-ccp-methodlist allow-execute="true" archive="true" 
    serviceurl="${ccp_url}">
        <script src="${cdn_url}/ccp/js/methodlistcontroller.js"></script>
    </d4s-ccp-methodlist>
</div>
