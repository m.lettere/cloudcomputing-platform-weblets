<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ include file="./init.jsp"%>

<div><d4s-ccp-executionhistory allow-archive="true" serviceurl="${ccp_url}">
    <script src="https://cdn.jsdelivr.net/npm/jszip@3.10.1/dist/jszip.min.js"></script>
    <script src="${cdn_url}/storage/d4s-storage.js"></script>
    <script src="${cdn_url}/ccp/js/executionhistorycontroller.js"></script>
    <script src="${cdn_url}/ccp/js/logterminalcontroller.js"></script>
</d4s-ccp-executionhistory></div>
