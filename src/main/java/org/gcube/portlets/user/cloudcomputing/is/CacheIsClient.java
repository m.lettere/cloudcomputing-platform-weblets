package org.gcube.portlets.user.cloudcomputing.is;

import java.io.Serializable;
import java.rmi.ServerException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.gcube.common.resources.gcore.ServiceEndpoint;

import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.cache.PortalCache;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Utility class to query EndPoints and search for AccessPoints from IS
 * 
 * @author Alfredo Oliviero (ISTI - CNR)
 */

public class CacheIsClient extends IsClient {
    private static com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(CacheIsClient.class);
    private static CacheIsClient _singleInstance;

    // Class to represent a cache entry

    static CacheIsClient getSingleton() {
        if (_singleInstance == null) {
            _singleInstance = new CacheIsClient();
        }
        return _singleInstance;
    }

    // Define the cache name
    private static final String CACHE_NAME = "my-cache";
    private PortalCache<Serializable, Serializable> portalCache = MultiVMPoolUtil.getCache(CACHE_NAME);

    // Method to get the cache key
    private String getCacheKey(String resourceName, String category) {
        return "IS_"+ resourceName + ":" + category;
    }

    // Main method with caching and locking
    public synchronized List<ServiceEndpoint> getEndpointsFromISWithCache(String resource_name, String category)
            throws ServerException {
        String key = getCacheKey(resource_name, category);
        long currentTime = System.currentTimeMillis();

        logger.info("Request received for resource: " + resource_name + ", category: " + category);

        synchronized (key.intern()) {
            logger.info("Acquired lock for key: " + key);
            CacheEntry cacheEntry = null;
            // Check if the entry is present in the cache and is valid
            try {
                cacheEntry = (CacheEntry) portalCache.get(key);
            } catch (ClassCastException e) {
                portalCache.remove(key);
            }

            if (cacheEntry != null) {
                if (currentTime - cacheEntry.timestamp <= TimeUnit.MINUTES.toMillis(10)) {
                    // If the cache value is still valid, return it
                    logger.info("Cache hit for key: " + key);
                    return cacheEntry.endpoints;
                } else {
                    // Otherwise, remove the expired entry
                    logger.info("Cache expired for key: " + key);
                    portalCache.remove(key);
                }
            }

            // If not present in the cache or is expired, make the request
            logger.info("Cache miss for key: " + key + ". Making request to external service.");
            List<ServiceEndpoint> endpoints;
            try {
                endpoints = super.getEndpointsFromIS(resource_name, category);
            } catch (ServerException e) {
                logger.error("Error fetching data from external service for key: " + key, e);
                throw e;
            }

            // Update the cache with the new result and timestamp
            logger.info("Updating cache for key: " + key);
            portalCache.put(key, new CacheEntry(endpoints, currentTime));

            return endpoints;
        }
    }

    // Metodo principale con caching e locking
    public List<ServiceEndpoint> getEndpointsFromIS(String resource_name, String category) throws ServerException {
        logger.info("@@@ cached getEndpointsFromIS: " + resource_name + ":" + category);

        return getEndpointsFromISWithCache(resource_name, category);
    }

}
