package org.gcube.portlets.user.cloudcomputing;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.ServerException;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.gcube.common.portal.PortalContext;
import org.gcube.portlets.user.cloudcomputing.config.CC_Config;
import org.gcube.portlets.user.cloudcomputing.is.IsClient;
import org.gcube.portlets.user.cloudcomputing.is.IsClientFactory;
import org.gcube.portlets.user.cloudcomputing.is.IsServerConfig;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class CC_Portlet extends MVCPortlet {
    private static com.liferay.portal.kernel.log.Log _log = LogFactoryUtil.getLog(CC_Portlet.class);

    private CC_Config configuration = null;

    public final static String IS_AUTH_RESOURCE_NAME = "IAM";
    public final static String IS_AUTH_CATEGORY = "Service";
    public final static String IS_AUTH_ENTRYPOINT = "d4science";

    public final static String IS_CCP_RESOURCE_NAME = "CCP";
    public final static String IS_CCP_CATEGORY = "DataAnalysis";
    public final static String IS_CDN_ENTRYPOINT = "cdn_url";
    public final static String IS_CCP_ENTRYPOINT = "ccp_url";

    public CC_Config getConfig(RenderRequest renderRequest)
            throws MalformedURLException, ServerException {
        if (configuration == null) {
            try {
                CC_Config config = new CC_Config();

                String currentContext = getCurrentContext(renderRequest);
                String encodedContext = getEncodedContext(currentContext);
                config.encoded_context = encodedContext;

                // config.redirect_url = redirect_url;
                HttpServletRequest httpReq = PortalUtil
                        .getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));
                String current_url = PortalUtil.getCurrentURL(renderRequest);
                // current_url = /group/devvre/cloudcomputing

                String absolute_current_url = PortalUtil.getAbsoluteURL(httpReq, current_url);
                // absolute_current_url =
                // https://next.dev.d4science.org/group/devvre/cloudcomputing

                config.redirect_url = absolute_current_url;

                URL base_url = new URL(PortalUtil.getAbsoluteURL(httpReq, "/"));
                String host = base_url.getHost();
                config.gateway = host;

                IsClient isclient = IsClientFactory.getSingleton();

                IsServerConfig auth_config = isclient.serviceConfigFromIS(IS_AUTH_RESOURCE_NAME,
                        IS_AUTH_CATEGORY, IS_AUTH_ENTRYPOINT);
                config.auth_url = auth_config.getServerUrl();

                IsServerConfig ccp_config = isclient.serviceConfigFromIS(IS_CCP_RESOURCE_NAME,
                        IS_CCP_CATEGORY, IS_CCP_ENTRYPOINT);
                config.ccp_url = ccp_config.getServerUrl();

                IsServerConfig cdn_config = isclient.serviceConfigFromIS(IS_CCP_RESOURCE_NAME,
                        IS_CCP_CATEGORY, IS_CDN_ENTRYPOINT);
                config.cdn_url = cdn_config.getServerUrl();

                configuration = config;
            } catch (Exception e) {
                e.printStackTrace();
                _log.error("cannot obtain configuration", e);
                return null;
            }
        }
        return configuration;
    }

    protected static String getCurrentContext(RenderRequest request) {
        long groupId = -1;
        try {
            groupId = PortalUtil.getScopeGroupId(request);
            return getCurrentContext(groupId);
        } catch (Exception e) {
            e.printStackTrace();
            _log.error(e);
        }
        return null;
    }

    protected static String getCurrentContext(long groupId) {
        try {
            PortalContext pContext = PortalContext.getConfiguration();
            return pContext.getCurrentScope("" + groupId);
        } catch (Exception e) {
            e.printStackTrace();
            _log.error(e);
        }
        return null;
    }

    protected static String getEncodedContext(String context) {
        return context.replace("/", "%2F");
    }

    @Override
    public void render(RenderRequest renderRequest, RenderResponse renderResponse)
            throws PortletException, IOException {
        // GroupManager gm = new LiferayGroupManager();
        try {

            // String encoded_context =
            // "%2Fd4science.research-infrastructures.eu%2FSoBigData%2FSoBigDataLab";
            // String gateway = "sobigdata.d4science.org";
            // String redirect_url = "https://sobigdata.d4science.org/group/sobigdatalab";
            // String url = "https://accounts.d4science.org/auth";

            CC_Config config = getConfig(renderRequest);

            renderRequest.setAttribute("config", config);

            renderRequest.setAttribute("encoded_context", config.encoded_context);
            renderRequest.setAttribute("gateway", config.gateway);
            renderRequest.setAttribute("redirect_url", config.redirect_url);
            renderRequest.setAttribute("auth_url", config.auth_url);
            renderRequest.setAttribute("ccp_url", config.ccp_url);
            renderRequest.setAttribute("cdn_url", config.cdn_url);

        } catch (Exception e) {
            e.printStackTrace();
            _log.error(e);
        }
        super.render(renderRequest, renderResponse);
    }

}
