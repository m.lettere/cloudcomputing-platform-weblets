package org.gcube.portlets.user.cloudcomputing.is;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.encryption.encrypter.StringEncrypter;
// import org.gcube.common.encryption.encrypter.StringEncrypter;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.resources.gcore.ServiceEndpoint.Property;

public class IsServerConfig {
    private String serverUrl;
    private String name;
    private String clientId;

    private Map<String, String> properties = new HashMap<String, String>();

    private String clientSecret;

    String CLIENT_CREDENTIALS = "client_credentials";

    private String grantType = CLIENT_CREDENTIALS;

    public Map<String, String> getProperties() {
        return this.properties;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public String getName() {
        return name;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getGrantType() {
        return grantType;
    }

    public boolean hasProperty(String key) {
        return this.properties.containsKey(key);
    }

    public String getProperty(String key) {
        return this.properties.get(key);
    }

    public IsServerConfig(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public IsServerConfig(String serverUrl, String name, String clientId, String clientSecret) {
        this.serverUrl = serverUrl;
        this.name = name;
        this.clientId = clientId;
        this.clientSecret = clientSecret;
    }

    public IsServerConfig(String serverUrl, String name, String clientId, String clientSecret,
            Map<String, String> properties) {
        this(serverUrl, name, clientId, clientSecret);
        this.properties = properties;
    }

    public IsServerConfig(ServiceEndpoint.AccessPoint accessPoint) throws Exception {
        this.serverUrl = accessPoint.address();
        this.name = accessPoint.name();
        this.clientId = accessPoint.username();
        //this.clientSecret = StringEncrypter.getEncrypter().decrypt(accessPoint.password());

        this.properties = new HashMap<String, String>();
        for (Property p : accessPoint.properties()) {
            String value = p.value();
            if (p.isEncrypted()) {
                value = StringEncrypter.getEncrypter().decrypt(value);
            }
            this.properties.put(p.name(), value);
        }
    }

    public IsServerConfig(String serverUrl, String name, String clientId, String clientSecret,
            Map<String, String> properties,
            String grantType) {
        this(serverUrl, name, clientId, clientSecret, properties);
        this.grantType = grantType;
    }
}
